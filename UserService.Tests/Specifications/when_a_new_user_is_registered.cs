using System;
using AggregateSource.Testing;
using NUnit.Framework;
using UserService.Domain;
using UserService.Tests.Specifications.Helpers;

namespace UserService.Tests.Specifications
{
    [TestFixture]
    class when_a_new_user_is_registered
    {
        [Test]
        public void an_active_user_is_created()
        {
            var id = new UserId(Guid.NewGuid());
            new ConstructorScenarioFor<User>(() => User.Register(id, "Jeroen", "Soeters", new DateTime(1980, 11, 7)))
                .Then(UserEvents.Registered(id, "Jeroen", "Soeters", new DateTime(1980, 11, 7), true))
                .Assert();
        }
    }
}