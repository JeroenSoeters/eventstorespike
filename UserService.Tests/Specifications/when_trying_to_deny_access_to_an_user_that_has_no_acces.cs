using System;
using AggregateSource.Testing;
using NUnit.Framework;
using UserService.Domain;
using UserService.Tests.Specifications.Helpers;

namespace UserService.Tests.Specifications
{
    [TestFixture]
    class when_trying_to_deny_access_to_an_user_that_has_no_acces
    {
        [Test]
        public void a_user_is_already_denied_access_exception_is_thrown()
        {
            var id = new UserId(Guid.NewGuid());
            new CommandScenarioFor<User>(User.Factory)
                .Given(UserEvents.Registered(id, "Victoria", "Brown", new DateTime(1979, 5, 2), true),
                    UserEvents.AccessDenied(id, "misbehaved on the website"))
                .When(sut => sut.DenyAccess("no good reason really"))
                .Throws(new UserIsAlreadyDeniedAccessException())
                .Assert();
        }
    }
}
