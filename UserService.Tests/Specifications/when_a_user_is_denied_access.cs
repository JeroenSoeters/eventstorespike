using System;
using AggregateSource.Testing;
using NUnit.Framework;
using UserService.Domain;
using UserService.Tests.Specifications.Helpers;

namespace UserService.Tests.Specifications
{
    [TestFixture]
    class when_a_user_is_denied_access
    {
        [Test]
        public void acces_is_denied()
        {
            var id = new UserId(Guid.NewGuid());
            new CommandScenarioFor<User>(User.Factory)
                .Given(UserEvents.Registered(id, "Alex", "Ulme", new DateTime(1988, 3, 1), true))
                .When(sut => sut.DenyAccess("a good reason"))
                .Then(UserEvents.AccessDenied(id, "a good reason"))
                .Assert();
        }
    }
}