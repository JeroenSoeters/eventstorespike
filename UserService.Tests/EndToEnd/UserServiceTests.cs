using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading;
using Microsoft.Owin.Testing;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace UserService.Tests.EndToEnd
{
    [TestFixture]
    public class UserServiceTests
    {
        private TestServer _server;
        private Process _ESProcess;
        private Process _projectionService;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            _server = TestServer.Create<Startup>();
            //_ESProcess = Process.Start(@"..\..\..\lib\EventStore-NET-v2.5.0rc2\EventStore.SingleNode.exe");
            Thread.Sleep(5000);
            _projectionService =
                Process.Start(
                    @"..\..\..\UserProjectionService\bin\Debug\UserProjectionService.exe");
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            //_ESProcess.CloseMainWindow();
            _projectionService.CloseMainWindow();
            _server.Dispose();
        }

        [Test, Ignore]
        public void GetResponseReturnsCorrectStatusCode()
        {
            var baseAddress = new Uri("http://localhost:9000");
            using (var client = _server.HttpClient)
            {
                client.BaseAddress = baseAddress;

                var result = client.GetAsync("/api/user").Result;
                Assert.IsTrue(
                    result.IsSuccessStatusCode,
                    "Actual status code: " + result.StatusCode);
            }
        }

        [Test, Ignore]
        public void UserRegisteredEventGetsPersisted()
        {
            var userId = Guid.NewGuid().ToString();

            var apiBaseAddress = new Uri("http://localhost:9000");
            using (var client = _server.HttpClient)
            {
                client.BaseAddress = apiBaseAddress;

                var result = client.PostAsync("/api/user/create",
                    new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("userId", userId), 
                        new KeyValuePair<string, string>("firstname", "Jeroen"),
                        new KeyValuePair<string, string>("lastName", "Soeters"),
                        new KeyValuePair<string, string>("dateOfBirth", "1980-11-07") 
                    })).Result;

                Assert.AreEqual(
                    HttpStatusCode.Created,
                    result.StatusCode,
                    "Actual status code for creating user: " + result.StatusCode);
            }

            var eventStoreBaseAddress = new Uri("http://localhost:2113");
            using (var client = new HttpClient())
            {
                client.BaseAddress = eventStoreBaseAddress;

                var result = client.GetAsync(string.Format("/streams/user-{0}", userId)).Result;

                Assert.IsTrue(
                    result.IsSuccessStatusCode,
                    "Actual status code for retrieving user stream: " + result.StatusCode);
            }
        }

        [Test, Ignore]
        public void test_interceptor()
        {
            Assert.Throws<NotImplementedException>(
                () =>
                {
                    var baseAddress = new Uri("http://localhost:9000");
                    using (var client = _server.HttpClient)
                    {
                        client.BaseAddress = baseAddress;

                        var postResult = client.PostAsync("/api/user/create",
                            new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("userId", Guid.NewGuid().ToString()),
                                new KeyValuePair<string, string>("firstname", "should"),
                                new KeyValuePair<string, string>("lastName", "fail"),
                                new KeyValuePair<string, string>("dateOfBirth", "1980-11-07")
                            })).Result; 

                        Assert.IsTrue(
                            postResult.IsSuccessStatusCode,
                            "Actual status code: " + postResult.StatusCode);
                    }
                });
        }
        
        [Test]
        public void can_create_new_user()
        {
            var userId = Guid.NewGuid();
            var baseAddress = new Uri("http://localhost:9000");
            using (var client = _server.HttpClient)
            {
                client.BaseAddress = baseAddress;

                var postResult = client.PostAsync("/api/user/create",
                    new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("userId", userId.ToString()),
                        new KeyValuePair<string, string>("firstname", "Jeroen"),
                        new KeyValuePair<string, string>("lastName", "Soeters"),
                        new KeyValuePair<string, string>("dateOfBirth", "1980-11-07")
                    })).Result;

                Assert.IsTrue(
                    postResult.IsSuccessStatusCode,
                    "Actual status code: " + postResult.StatusCode);

                // wait for user to be created
                Thread.Sleep(2000);

                var getResult = client.GetAsync(string.Format("/api/user/{0}", userId)).Result;

                dynamic user = JObject.Parse(getResult.Content.ReadAsStringAsync().Result);

                string firstName = user.FirstName;
                string surname = user.Surname;
                DateTime dateOfBirth = user.DateOfBirth;

                Assert.AreEqual("Jeroen", firstName);
                Assert.AreEqual("Soeters", surname);
                Assert.AreEqual(new DateTime(1980, 11, 7), dateOfBirth);
            }
        }
    }
}
