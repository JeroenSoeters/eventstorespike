﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using FluentAssertions;
using NUnit.Framework;
using UserService.CommandHandlers;
using UserService.Commands;
using UserService.Infrastructure;

namespace UserService.Tests.Bus
{
    [TestFixture]
    class When_a_command_gets_published_to_a_bus
    {
        private TestCommmandHandler _handler;
        private TestCommand _command;

        [Test]
        public void Then_the_handle_method_on_the_returned_commandhandler_is_invoked_with_the_provided_command()
        {
            // given
            var commandId = Guid.NewGuid();
            var bus = new FakeBus();
            _handler = new TestCommmandHandler();
            _command = new TestCommand(commandId);
            bus.RegisterHandler<TestCommand>(c => _handler.Handle(c));
            
            // when
            bus.Send(_command);

            // then
            _handler.Ids.Single().Should().Be(commandId);
        }
    }

    public class TestCommand: ICommand
    {
        public Guid Id { get; private set; }

        public TestCommand(Guid id)
        {
            Id = id;
        }
    }

    public class TestCommmandHandler: ICommandHandler<TestCommand>
    {
        public List<Guid> Ids { get; set; }

        public TestCommmandHandler()
        {
            Ids = new List<Guid>();
        }

        public void Handle(TestCommand command)
        {
            Ids.Add(command.Id);
        }
    }
}
