﻿using System.Linq;
using Castle.DynamicProxy;

namespace UserService
{
    static class InvocationExtensions
    {
        public static bool IsTransactional(this IInvocation invocation)
        {
            return invocation.MethodInvocationTarget.CustomAttributes.Any(
                attr => attr.AttributeType == typeof (TransactionalAttribute));
        }
    }
}