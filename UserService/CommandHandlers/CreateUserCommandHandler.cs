using AggregateSource;
using UserService.Commands;
using UserService.Domain;

namespace UserService.CommandHandlers
{    
    public class CreateUserCommandHandler : ICommandHandler<CreateUserCommand>
    {
        private readonly IRepository<User> _repository;

        public CreateUserCommandHandler(IRepository<User> repository)
        {
            _repository = repository;
        }

        [Transactional]
        public virtual void Handle(CreateUserCommand command)
        {
            var user = User.Register(new UserId(command.UserId), command.FirstName, command.Lastname,
                command.DateOFBirth);
            _repository.Add(string.Format("user-{0}", command.UserId), user);
        }
    }
}
