using System;
using System.Linq;
using AggregateSource;
using Raven.Client;
using Raven.Client.Document;
using UserProjectionService;
using UserService.Controllers;

namespace UserService.Infrastructure
{
    public class RavenReadModelRepository<T> : IReadModelRepository<UserReadModel>
    {
        private readonly IDocumentStore _store;

        public RavenReadModelRepository()
        {
            _store = new DocumentStore
            {
                Url = "http://localhost:8080"
            };
            _store.Initialize();
        }

        public Optional<UserReadModel> GetById(Guid id)
        {
            UserReadModel user;
            using (var session = _store.OpenSession("users"))
            {
                user = session.Query<UserReadModel>().SingleOrDefault(model => model.UserId == id);
            }
            return user == null 
                ? new Optional<UserReadModel>()
                : new Optional<UserReadModel>(user);
        }
    }
}