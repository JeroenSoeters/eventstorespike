using System;
using System.Collections.Generic;
using UserService.Bus;
using UserService.Commands;

namespace UserService.Infrastructure
{
    public class FakeBus : IBus
    {
        private readonly Dictionary<Type, List<Action<ICommand>>> _routes = new Dictionary<Type, List<Action<ICommand>>>();

        public void RegisterHandler<T>(Action<T> handler) where T : ICommand
        {
            List<Action<ICommand>> handlers;
            if (!_routes.TryGetValue(typeof(T), out handlers))
            {
                handlers = new List<Action<ICommand>>();
                _routes.Add(typeof(T), handlers);
            }
            handlers.Add(DelegateAdjuster.CastArgument<ICommand, T>(x => handler(x)));
        }

        public void Send<T>(T command) where T : ICommand
        {
            List<Action<ICommand>> handlers;
            if (_routes.TryGetValue(typeof(T), out handlers))
            {
                if (handlers.Count != 1) throw new InvalidOperationException("cannot send to more than one handler");
                handlers[0](command);
            }
            else
            {
                throw new InvalidOperationException("no handler registered");
            }
        }
    }
}