﻿using System;
using UserService.Domain.Common;
using UserService.Messaging;

namespace UserService.Domain
{
    public class User : AggregateRootEntity
    {
        private UserId _id;
        private bool _hasAccess = true;

        public static readonly Func<User> Factory = () => new User();

        User()
        {
            Register<UserRegisteredEvent>(When);
            Register<AccessDeniedToUserEvent>(When);
        }

        void When(UserRegisteredEvent @event)
        {
            _id = new UserId(@event.UserId);
        }

        void When(AccessDeniedToUserEvent @event)
        {
            _hasAccess = false;
        }
 
        public static User Register(UserId userId, string firstName, string lastName, DateTime dateOfBirth)
        {
            var user = Factory();
            user.ApplyChange(UserEvents.Registered(userId, firstName, lastName, dateOfBirth, true));
            return user;
        }

        public void DenyAccess(string reason)
        {
            if (!_hasAccess)
                throw new UserIsAlreadyDeniedAccessException();

            ApplyChange(UserEvents.AccessDenied(_id, reason));    
        }
    }
}