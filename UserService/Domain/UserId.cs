﻿using System;

namespace UserService.Domain
{
    public struct UserId
    {
        private readonly Guid _value;

        public UserId(Guid value)
        {
            _value = value;
        }

        public bool Equals(UserId other)
        {
            return _value.Equals(other._value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is UserId && Equals((UserId) obj);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public static implicit operator Guid(UserId id)
        {
            return id._value;
        }
    }
}