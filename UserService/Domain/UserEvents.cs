﻿using System;
using UserService.Messaging;

namespace UserService.Domain
{
    public static class UserEvents
    {
        public static UserRegisteredEvent Registered(Guid id, string firstName, string lastName, DateTime dateOfBirth, bool hasAccess)
        {
            return new UserRegisteredEvent(id, firstName, lastName, dateOfBirth, hasAccess);
        }

        public static AccessDeniedToUserEvent AccessDenied(UserId id, string reason)
        {
            return new AccessDeniedToUserEvent(id, reason);
        }
    }
}