﻿using System;
using UserService.Commands;

namespace UserService.Bus
{
    public interface IBus
    {
        void Send<T>(T command) where T : ICommand;

        void RegisterHandler<T>(Action<T> handler) where T : ICommand;
    }
}