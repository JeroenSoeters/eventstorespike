﻿using System;
using System.Linq;
using System.Text;
using AggregateSource.EventStore;
using Castle.DynamicProxy;
using EventStore.ClientAPI;
using Newtonsoft.Json;
using UserService.Domain;

namespace UserService
{
    class TransactionalInterceptor: IInterceptor
    {
        private readonly Repository<User> _repository;

        public TransactionalInterceptor(Repository<User> repository)
        {
            _repository = repository;
        }

        public void Intercept(IInvocation invocation)
        {
            invocation.Proceed();

            if (!invocation.IsTransactional()) return;

            var user = _repository.UnitOfWork.GetChanges().Single();
            var events =
                user.Root.GetChanges()
                    .Select(e => new EventData(Guid.NewGuid(), e.GetType().Name, true,
                        Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(e)), null));
            _repository.Connection.Connect();
            bool success = _repository.Connection.AppendToStream(user.Identifier, user.ExpectedVersion,
                events).NextExpectedVersion == 0;
            _repository.Connection.Close();
        }
    }
}
