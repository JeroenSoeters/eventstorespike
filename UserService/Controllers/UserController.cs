using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using UserProjectionService;
using UserService.Bus;
using UserService.Commands;
using UserService.Infrastructure;

namespace UserService.Controllers
{
    public class UserController : ApiController
    {
        private readonly IBus _bus;
        private readonly IReadModelRepository<UserReadModel> _repository;

        public UserController(IBus bus, IReadModelRepository<UserReadModel> repository)
        {
            _bus = bus;
            _repository = repository;
        }

        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            var user = _repository.GetById(Guid.Parse(id));

            return user.HasValue
                ? new HttpResponseMessage(HttpStatusCode.Found)
                {
                    Content = new JsonContent(new
                    {
                        FirstName = user.Value.FirstName,
                        Surname = user.Value.Surname,
                        DateOfBirth = user.Value.DateOfBirth
                    })
                }
                : new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        [HttpPost]
        async public Task<HttpResponseMessage> Create(HttpRequestMessage request){
            var formData = await request.Content.ReadAsFormDataAsync();
            var command = new CreateUserCommand(
                Guid.Parse(formData["userId"]), 
                formData["firstName"],
                formData["lastName"],
                DateTime.Parse(formData["dateOFBirth"]));

            try
            {
                _bus.Send(command);
                return new HttpResponseMessage(HttpStatusCode.Created);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}