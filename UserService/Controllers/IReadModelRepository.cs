using System;
using AggregateSource;

namespace UserService.Controllers
{
    public interface IReadModelRepository<TReadModel>
    {
        Optional<TReadModel> GetById(Guid id);
    }
}