using System;

namespace UserService.Commands
{
    public class CreateUserCommand : ICommand
    {
        public CreateUserCommand(Guid userId, string firstName, string lastname, DateTime dateOfBirth)
        {
            UserId = userId;
            FirstName = firstName;
            Lastname = lastname;
            DateOFBirth = dateOfBirth;
        }

        public Guid UserId { get; private set; }
        public string FirstName { get; private set; }
        public string Lastname { get; private set; }
        public DateTime DateOFBirth { get; private set; }
    }
}