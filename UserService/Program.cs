﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Dependencies;
using AggregateSource;
using AggregateSource.EventStore;
using AggregateSource.EventStore.Resolvers;
using Autofac;
using Autofac.Extras.DynamicProxy2;
using Autofac.Integration.WebApi;
using EventStore.ClientAPI;
using Microsoft.Owin.Hosting;
using Owin;
using UserProjectionService;
using UserService.Bus;
using UserService.CommandHandlers;
using UserService.Commands;
using UserService.Controllers;
using UserService.Domain;
using UserService.Infrastructure;
using UserService.Serialization;

namespace UserService
{
    class Program
    {
        static void Main(string[] args)
        {
            const string baseAddress = "http://localhost:9000/"; 

            // Start OWIN host 
            using (WebApp.Start<Startup>(url: baseAddress)) 
            { 
                // Create HttpCient and make a request to api/values 
                var client = new HttpClient(); 

                var response = client.GetAsync(baseAddress + "api/user").Result; 

                Console.WriteLine(response); 
                Console.WriteLine(response.Content.ReadAsStringAsync().Result); 
            } 

            Console.ReadLine(); 
        } 
    }

    public class Startup
    {
        private readonly IContainer _container;
        private readonly Repository<User> _userRepository;
        private static readonly IEventStoreConnection EventstoreConnection;

        static Startup()
        {
            EventstoreConnection = EventStoreConnection.Create(new IPEndPoint(IPAddress.Loopback, 1113));
        }

        public Startup()
        {
            _userRepository = CreateUserRepository();

            var builder = new ContainerBuilder();

            builder.Register(c => new TransactionalInterceptor(_userRepository));

            builder.Register(c => new CreateUserCommandHandler(_userRepository))
                .As<ICommandHandler<CreateUserCommand>>()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof (TransactionalInterceptor));
            
            builder.Register(c =>
            {
                var bus = new FakeBus();
                var createUserCommandHandler = c.Resolve<ICommandHandler<CreateUserCommand>>();
                bus.RegisterHandler<CreateUserCommand>(createUserCommandHandler.Handle);
                return bus;
            }).As<IBus>();

            builder.RegisterType<RavenReadModelRepository<UserReadModel>>().As<IReadModelRepository<UserReadModel>>();
            
            builder.RegisterType<UserController>();

            _container = builder.Build();
        }

        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );
            config.DependencyResolver = new AutofacWebApiDependencyResolver(_container);
            appBuilder.UseWebApi(config);
        }

        private static Repository<User> CreateUserRepository()
        {
            var userEventReaderConfiguration = new EventReaderConfiguration(new SliceSize(1), new UserDeserializer(),
                new PassThroughStreamNameResolver(), new NoStreamUserCredentialsResolver());
            return new Repository<User>(User.Factory, new UnitOfWork(),
                EventstoreConnection,
                userEventReaderConfiguration);
        }
    }
}
