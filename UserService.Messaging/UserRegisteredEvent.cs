﻿using System;

namespace UserService.Messaging
{
    public class UserRegisteredEvent
    {
        public UserRegisteredEvent(Guid userId, string firstName, string surname, DateTime dateOfBirth, bool active)
        {
            UserId = userId;
            FirstName = firstName;
            Surname = surname;
            DateOfBirth = dateOfBirth;
            Active = active;
        }

        public readonly Guid UserId;
        public readonly string FirstName;
        public readonly string Surname;
        public readonly DateTime DateOfBirth;
        public readonly bool Active;
    }
}