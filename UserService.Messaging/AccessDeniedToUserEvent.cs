﻿using System;

namespace UserService.Messaging
{
    public class AccessDeniedToUserEvent
    {
        public AccessDeniedToUserEvent(Guid userId, string reason)
        {
            UserId = userId;
            Reason = reason;
        }

        public readonly Guid UserId;
        public readonly string Reason;
    }
}