﻿using Raven.Client;
using Raven.Client.Document;
using UserService.Messaging;

namespace UserProjectionService
{
    public class UserProjection
    {
        private readonly IDocumentStore _store;

        public UserProjection()
        {
            _store = new DocumentStore { Url = "http://localhost:8080" };
            _store.Initialize();
        }

        public void Handle(UserRegisteredEvent @event)
        {
            using (var session = _store.OpenSession("users"))
            {
                var userReadModel = new UserReadModel
                {
                    UserId = @event.UserId,
                    FirstName = @event.FirstName,
                    Surname = @event.Surname,
                    DateOfBirth = @event.DateOfBirth
                };
                session.Store(userReadModel);
                session.SaveChanges();
            }
        }
    }
}