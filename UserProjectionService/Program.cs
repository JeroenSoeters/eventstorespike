﻿using System;
using System.Net;
using System.Text;
using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using Raven.Imports.Newtonsoft.Json;
using UserService.Messaging;

namespace UserProjectionService
{
    class Program
    {
        private static void Main(string[] args)
        {
            using (var connection = EventStoreConnection.Create(new IPEndPoint(IPAddress.Loopback, 1113)))
            {
                connection.Connect();
                connection.SubscribeToAll(false,
                    EventAppeared,  null, new UserCredentials("admin", "changeit"));

                Console.WriteLine("Subscribed to all streams...");
                Console.ReadKey();
            }
        }

        private static void EventAppeared(EventStoreSubscription subscription, ResolvedEvent resolvedEvent)
        {
            Console.WriteLine(resolvedEvent.Event.EventType + " received");
            if (resolvedEvent.Event.EventType == "UserRegisteredEvent")
            {
                var projection = new UserProjection();
                projection.Handle(JsonConvert.DeserializeObject<UserRegisteredEvent>(Encoding.UTF8.GetString(resolvedEvent.Event.Data), new JsonSerializerSettings
                {
                    ConstructorHandling = ConstructorHandling.Default
                }));
            }
        }
    }
}
