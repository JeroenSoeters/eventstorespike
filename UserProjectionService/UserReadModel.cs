﻿using System;

namespace UserProjectionService
{
    public class UserReadModel
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}